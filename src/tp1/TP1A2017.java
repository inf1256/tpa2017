package tp1;
/*
 ceci est une solution qu'un étudiant moyen ayant suivi le cours
 aurait pu proposer. ici les informations saisies ne sont pas validées.
 */
import java.util.Locale;
import java.util.Scanner;

public class TP1A2017 {
    //constantes
    final static double TRANCHE1 = 50.0;
    final static double TRANCHE2 = 100.0;
    final static double TRANCHE3 = 150.0;
    final static double TRANCHE4 = 200.0;
    final static double TAUX_TRANCHE1 = 0.0;
    final static double TAUX_TRANCHE2 = 0.15;
    final static double TAUX_TRANCHE3 = 0.25;
    final static double TAUX_TRANCHE4 = 0.35;
    final static double TAUX_TRANCHE5 = 0.50;
    final static String MASCULIN = "M";
    final static String FEMININ = "F";

    public static void main(String[] args){
        Scanner clavier = new Scanner(System.in);
        clavier.useLocale(Locale.CANADA_FRENCH); //Pour utiliser des virgules
        clavier.useDelimiter(System.lineSeparator());
        //clavier.useLocale(Locale.US); pour .
        System.out.println("BIENVENU AU LOGICIEL De GESTION DE MASSE SALARIALE:");
        System.out.print("Saisissez le nom de l'employé: ");
        String nom = clavier.nextLine();
        System.out.print("Saisissez le prénom de l'employé: ");
        String prenom = clavier.nextLine();
        System.out.print("Saisissez le taux horaire (en $ / h) : ");
        double taux = clavier.nextDouble();
        System.out.print("Saisissez le nombre d'heure travaillées : ");
        double nombreHeures = clavier.nextDouble();
        clavier.nextLine();//apres nextDouble(), etc, il faut nextLine lire la caractere de fin de ligne
        System.out.print("Saisissez le département d'attache: ");
        String departement = clavier.nextLine();
        System.out.print("Saisissez l'age de l'employé : ");
        int age = clavier.nextInt();
        clavier.nextLine();
        System.out.print("Saisissez le sexe de l'employé: ");
        String sexe = clavier.nextLine();
        //calcul
        double salaireBrut = taux * nombreHeures;
        double deductionImpot = 0.0;
        double montantATaxer = salaireBrut;
        double tr1 = 0.0;
        if ((montantATaxer>=0)&& (montantATaxer<=TRANCHE1)) {
            tr1 = montantATaxer;
            montantATaxer = 0.0;
        }else if (montantATaxer>TRANCHE1){
            tr1 = TRANCHE1;
            montantATaxer = montantATaxer - TRANCHE1;
        }

        double tr2 = 0.0;
        if ((montantATaxer>=0)&& (montantATaxer<=TRANCHE2)) {
            tr2 = montantATaxer;
            montantATaxer = 0.0;
        }else if (montantATaxer>TRANCHE2){
            tr2 = TRANCHE2;
            montantATaxer = montantATaxer - TRANCHE2;
        }

        double tr3 = 0.0;
        if ((montantATaxer>=0)&& (montantATaxer<=TRANCHE3)) {
            tr3 = montantATaxer;
            montantATaxer = 0.0;
        }else if (montantATaxer>TRANCHE3){
            tr3 = TRANCHE3;
            montantATaxer = montantATaxer - TRANCHE3;
        }


        double tr4 = 0.0;
        if ((montantATaxer>=0)&& (montantATaxer<=TRANCHE4)) {
            tr4 = montantATaxer;
            montantATaxer = 0.0;
        }else if (montantATaxer>TRANCHE4){
            tr4 = TRANCHE4;
            montantATaxer = montantATaxer - TRANCHE4;
        }

        double tr5 = montantATaxer;

        deductionImpot = (tr1 * TAUX_TRANCHE1) + (tr2 * TAUX_TRANCHE2) + (tr3 * TAUX_TRANCHE3);
        deductionImpot = deductionImpot + (tr4 * TAUX_TRANCHE4) + (tr5 * TAUX_TRANCHE5);
        double salaireNet = salaireBrut - deductionImpot;

        //Affichage -- utiliser System.out.format() pour formatter affichage
        System.out.println("\n RESULTATS DE La SAISIE:");
        System.out.println("Nom : "+prenom.substring(0,1)+". "+nom);
        System.out.println("Salaire brut : "+ salaireBrut);
        System.out.println("Salaire brut : "+ deductionImpot);
        System.out.println("Salaire net : "+ salaireNet);

    }
}
