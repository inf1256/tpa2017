package tp2;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class TP2A2017 {
        final static int MAX_EMPLOYES = 10;
        final static int NOMBRE_CENT = 100;
        final static double TRANCHE1 = 50.0;
        final static double TRANCHE2 = 100.0;
        final static double TRANCHE3 = 150.0;
        final static double TRANCHE4 = 200.0;
        final static double TAUX_TRANCHE1 = 0.0;
        final static double TAUX_TRANCHE2 = 0.15;
        final static double TAUX_TRANCHE3 = 0.25;
        final static double TAUX_TRANCHE4 = 0.35;
        final static double TAUX_TRANCHE5 = 0.50;
        final static String MASCULIN = "M";
        final static String FEMININ = "F";
        int nombreEmploye = 0;
        String[] nomTab = new String[MAX_EMPLOYES];
        String[] prenomTab = new String[MAX_EMPLOYES];
        double[] tauxTab = new double[MAX_EMPLOYES];
        double[] nombreHeuresTab = new double[MAX_EMPLOYES];
        String[] departementTab = new String[MAX_EMPLOYES];
        int[] ageTab = new int[MAX_EMPLOYES];
        String[] sexeTab = new String[MAX_EMPLOYES];
        double[] salaireBrutTab = new double[MAX_EMPLOYES];
        double[] salaireNetTab = new double[MAX_EMPLOYES];
        double[] impotTab = new double[MAX_EMPLOYES];
        Scanner clavier = new Scanner(System.in);

    private void saisirNouvelEmploye(){

        System.out.println("Saisie des information d'un employé:");
        System.out.print("Saisissez le nom de l'employé: ");
        String nom = this.clavier.nextLine();
        System.out.print("Saisissez le prénom de l'employé: ");
        String prenom = this.clavier.nextLine();
        System.out.print("Saisissez le taux horaire (en $ / h) : ");
        double taux = this.clavier.nextDouble();
        System.out.print("Saisissez le nombre d'heures travaillées : ");
        double nombreHeures = this.clavier.nextDouble();
        clavier.nextLine();//apres nextDouble(), etc, il faut nextLine lire la caractere de fin de ligne
        System.out.print("Saisissez le département d'attache: ");
        String departement = this.clavier.nextLine();
        System.out.print("Saisissez l'age de l'employé : ");
        int age = this.clavier.nextInt();
        this.clavier.nextLine();
        System.out.print("Saisissez le sexe de l'employé: ");
        String sexe = this.clavier.nextLine();
        //calcul
        double salaireBrut = taux * nombreHeures;
        double deductionImpot = 0.0;
        double montantATaxer = salaireBrut;
        double tr1 = 0.0;
        if ((montantATaxer>=0)&& (montantATaxer<=TRANCHE1)) {
            tr1 = montantATaxer;
            montantATaxer = 0.0;
        }else if (montantATaxer>TRANCHE1){
            tr1 = TRANCHE1;
            montantATaxer = montantATaxer - TRANCHE1;
        }

        double tr2 = 0.0;
        if ((montantATaxer>=0)&& (montantATaxer<=TRANCHE2)) {
            tr2 = montantATaxer;
            montantATaxer = 0.0;
        }else if (montantATaxer>TRANCHE2){
            tr2 = TRANCHE2;
            montantATaxer = montantATaxer - TRANCHE2;
        }

        double tr3 = 0.0;
        if ((montantATaxer>=0)&& (montantATaxer<=TRANCHE3)) {
            tr3 = montantATaxer;
            montantATaxer = 0.0;
        }else if (montantATaxer>TRANCHE3){
            tr3 = TRANCHE3;
            montantATaxer = montantATaxer - TRANCHE3;
        }


        double tr4 = 0.0;
        if ((montantATaxer>=0)&& (montantATaxer<=TRANCHE4)) {
            tr4 = montantATaxer;
            montantATaxer = 0.0;
        }else if (montantATaxer>TRANCHE4){
            tr4 = TRANCHE4;
            montantATaxer = montantATaxer - TRANCHE4;
        }

        double tr5 = montantATaxer;

        deductionImpot = (tr1 * TAUX_TRANCHE1) + (tr2 * TAUX_TRANCHE2) + (tr3 * TAUX_TRANCHE3);
        deductionImpot = deductionImpot + (tr4 * TAUX_TRANCHE4) + (tr5 * TAUX_TRANCHE5);
        double salaireNet = salaireBrut - deductionImpot;
        //enregistrement dans les tables
        this.nomTab[this.nombreEmploye] = nom;
        this.prenomTab[this.nombreEmploye] = prenom;
        this.tauxTab[this.nombreEmploye] = taux;
        this.nombreHeuresTab[this.nombreEmploye] = nombreHeures;
        this.departementTab[this.nombreEmploye] = departement;
        this.ageTab[this.nombreEmploye] = age;
        this.sexeTab[this.nombreEmploye] = sexe;
        this.salaireBrutTab[this.nombreEmploye] = salaireBrut;
        this.impotTab[this.nombreEmploye] = deductionImpot;
        this.salaireNetTab[this.nombreEmploye] = salaireNet;
        this.nombreEmploye++;
        //Affichage -- utiliser System.out.format() pour formatter affichage
        System.out.println("\n RESULTATS DE La SAISIE:");
        System.out.println("Nom : "+prenom.substring(0,1)+". "+nom);
        System.out.println("Salaire brut : "+ salaireBrut);
        System.out.println("Salaire brut : "+ deductionImpot);
        System.out.println("Salaire net : "+ salaireNet);
    }

    private double pourcentageHommes(String[] sexT){//pourcentage femme deduit par difference
        int nbHommes =0;
        for(int i=0; i<this.nombreEmploye;i++){
            if(sexT[i].equalsIgnoreCase(MASCULIN)) {
                nbHommes = nbHommes + 1;
            }
        }
        double pourcentHommes =0;

        if(this.nombreEmploye>0){
            pourcentHommes = (((double)nbHommes) / this.nombreEmploye)* NOMBRE_CENT;
        }

        return(pourcentHommes);
    }

    private double salaireTotalBrut(double[] salBrut){
        double totBrut =0;
        for(int i=0; i<this.nombreEmploye;i++){
            totBrut = totBrut + salBrut[i];
        }
        return(totBrut);
    }

    private double salaireTotalNet(double[] salNet){
        double totNet =0;
        for(int i=0; i<this.nombreEmploye;i++){
            totNet = totNet + salNet[i];
        }
        return(totNet);
    }

    private double ageMoyen(int[] ageT){
        int totAge =0;
        for(int i=0; i<this.nombreEmploye;i++){
            totAge = totAge + ageT[i];
        }
        double ageMo = 0.0;
        if(this.nombreEmploye>0){
            ageMo = ((double)totAge) / this.nombreEmploye;
        }
        return(ageMo);
    }

    private double salaireBrutTotalDepartement(String dptCourant, String[] tabDpt, double[] tabSalBrut){

        double totSalBrutDpt = 0.0;
        for(int i=0; i<this.nombreEmploye;i++){
            if(tabDpt[i].equalsIgnoreCase(dptCourant)){
                totSalBrutDpt = totSalBrutDpt + tabSalBrut[i];
            }
        }
        return totSalBrutDpt;
    }

    private double salaireNetTotalDepartement(String dptCourant, String[] tabDpt, double[] tabSalNet){

        double totSalNetDpt = 0.0;
        for(int i=0; i<this.nombreEmploye;i++){
            if(tabDpt[i].equalsIgnoreCase(dptCourant)){
                totSalNetDpt = totSalNetDpt + tabSalNet[i];
            }
        }
        return totSalNetDpt;
    }

    private void afficherInfo(){
        int choixMenu2 = 0;
        System.out.println("Veuillez choisir un menu svp");
        System.out.println(" 1. Afficher le pourcentage hommes et femmes");
        System.out.println(" 2. Afficher le salaire total brut");
        System.out.println(" 3. Afficher le salaire total net");
        System.out.println(" 4. Afficher le salaire total brut par departement");
        System.out.println(" 5. Afficher le salaire total net par departement");
        System.out.println(" 6. Afficher age moyen");
        System.out.println("Saisissez une option");
        choixMenu2 = this.clavier.nextInt();
        this.clavier.nextLine();
        String dptCourrant ;
        String[] copieDepTab;
        switch (choixMenu2){
            case 1:
                 double phommes = this.pourcentageHommes(this.sexeTab);
                 double pfemmes = ((double)NOMBRE_CENT) - phommes;
                 System.out.println( phommes +" % hommes et "+ pfemmes +" % femmes sur "+ this.nombreEmploye+" employes");
                 break;
            case 2: System.out.println("Salaire  total brut = "+ this.salaireTotalBrut(this.salaireBrutTab));
                   break;
            case 3: System.out.println("Salaire total net = "+ this.salaireTotalNet(this.salaireNetTab));
                   break;
            case 4:
                System.out.println("Voici le salaire brut total par département");
                copieDepTab =this.departementTab.clone();
                if(this.nombreEmploye>1) { //voir https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html
                    Arrays.sort(copieDepTab, 0, this.nombreEmploye);//on trie les départements pour que les départements identiques soient cote à cote
                }
                dptCourrant = "";
                for(int i=0;i<this.nombreEmploye;i++){
                    if(!copieDepTab[i].equalsIgnoreCase(dptCourrant)){//dpt pas encore traite
                        dptCourrant = copieDepTab[i];
                        System.out.println(this.salaireBrutTotalDepartement(dptCourrant,this.departementTab, this.salaireBrutTab) + " Pour : "+dptCourrant);
                    }
                }

                   break;
            case 5:
                System.out.println("Voici le salaire net total par département:");
                copieDepTab =this.departementTab.clone();
                if(this.nombreEmploye>1) {
                    Arrays.sort(copieDepTab, 0, this.nombreEmploye);//on trie les départements pour que les départements identiques soient cote à cote
                }
                dptCourrant = "";
                for(int i=0;i<this.nombreEmploye;i++){
                    if(!copieDepTab[i].equalsIgnoreCase(dptCourrant)){//dpt pas encore traité
                        dptCourrant = copieDepTab[i];
                        System.out.println(this.salaireNetTotalDepartement(dptCourrant,this.departementTab, this.salaireNetTab) + " Pour : "+dptCourrant);
                    }
                }

                   break;
            case 6: System.out.println("Age moyen = "+ this.ageMoyen(this.ageTab));
                   break;
            default: System.out.println("Choix de menu invalide");

        }
    }
    public static  void main(String[] args){

        System.out.println("BIENVENU AU LOGICIEL DE GESTION DE MASSE SALARIALE: TP2");
        TP2A2017 tp2 = new TP2A2017();
        tp2.clavier.useLocale(Locale.CANADA_FRENCH);
        tp2.clavier.useDelimiter(System.lineSeparator());
        int choixMenu = 0;
        while (choixMenu != 3){
            System.out.println("");//une ligne pour faire une espace
            System.out.println("Veuillez choisir un menu svp");
            System.out.println(" 1. Ajouter un nouvel employé");
            System.out.println(" 2. Afficher une information");
            System.out.println(" 3. Terminer");
            choixMenu = tp2.clavier.nextInt();
            tp2.clavier.nextLine();
            switch (choixMenu){
                case 1: tp2.saisirNouvelEmploye();
                         break;
                case 2: tp2.afficherInfo();
                   break;
                case 3: break;
                default: System.out.println("Option invalide");
            }

        }
        System.out.println("Fin du programme");
    }
}
